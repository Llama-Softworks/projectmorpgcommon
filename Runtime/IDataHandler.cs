using LiteNetLib;

namespace Llama.MORPG.Common {
	public interface IDataHandler {
		void OnData(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod);
	}
}
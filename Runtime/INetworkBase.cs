﻿using LiteNetLib;

namespace Llama.MORPG.Common {
	public interface INetworkBase {
		void OnConnectionEvent(NetPeer peer);
		void OnDisconnectEvent(NetPeer peer, DisconnectInfo disconnectInfo);
		void Start();
		void Update();
		void Shutdown();
	}
}
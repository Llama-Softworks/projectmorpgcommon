using LiteNetLib;
using Llama.MORPG.Common.Packets;

namespace Llama.MORPG.Common {
	public abstract class NetworkBase : INetworkBase {
		public readonly NetPeer Peer;
		public readonly NetManager Client;
		public readonly string ConnectionAddress;
		private readonly int Port;
		public readonly EventBasedNetListener Listener = new EventBasedNetListener();

		public NetworkBase(IDataHandler dataHandler, string ip = "localhost", int port = 43221) {
			Listener.NetworkReceiveEvent += dataHandler.OnData;
			ConnectionAddress = ip;
			Port = port;
			Client = new NetManager(Listener);
			Client.Start();
			Peer = Client.Connect(ConnectionAddress, Port, "");
			Listener.PeerConnectedEvent += OnConnectionEvent;
			Listener.PeerDisconnectedEvent += OnDisconnectEvent;
			var retries = 0;
			while (Peer.ConnectionState != ConnectionState.Connected) {
				//Wait for connection
				//If connection takes longer than 15 seconds we presume crashed
				if (retries == 5)
					return;
				retries++;
				Peer = Client.Connect(ConnectionAddress, Port, "");
			}
		}

		public abstract void OnConnectionEvent(NetPeer peer);

		public abstract void OnDisconnectEvent(NetPeer peer, DisconnectInfo disconnectInfo);

		public abstract void Start();

		public void Update() {
			Client.PollEvents();
		}

		public void Shutdown() {
			Client.Stop();
		}

		public void SendData(Packet packet, DeliveryMethod deliveryMethod) {
			Peer.Send(packet.GetBytes(), deliveryMethod);
		}
	}
}
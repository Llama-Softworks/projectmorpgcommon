using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using LiteNetLib.Utils;
using Llama.MORPG.Common.Packets;

namespace Llama.MORPG.Common {
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	public static class NetworkUtilities {
		private static readonly BinaryFormatter Formatter = new BinaryFormatter();

		public static string ConvertDNSToIP(string dns) {
			return Dns.GetHostAddresses(dns)[0].ToString();
		}

		public static byte[] GetBytes(this string str) {
			var bytes = new byte[str.Length * sizeof(char)];
			Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		public static byte[] GetBytes(this Packet packet, bool small = false) {
			byte[] data = small ? new byte[500] : new byte[1024];

			var stream = new MemoryStream(data);
			Formatter.Serialize(stream, packet);
			return data;
		}

		public static Packet GetPacket(this NetDataReader dataReader) {
			var data = new byte[dataReader.UserDataSize];
			dataReader.GetBytes(data, dataReader.UserDataSize);
			var stream = new MemoryStream(data);
			return Formatter.Deserialize(stream) as Packet;
		}

		public static string GetLocalIPAddress() {
			var localIP = "";
			IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (IPAddress ip in host.AddressList) {
				if (ip.AddressFamily != AddressFamily.InterNetwork) continue;
				localIP = ip.ToString();
				break;
			}

			return localIP;
		}

		public static string GetExternalIPAddress() {
			return new WebClient().DownloadString("http://icanhazip.com");
		}
	}
}
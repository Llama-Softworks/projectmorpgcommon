using System;

namespace Llama.MORPG.Common.Packets {
    [Serializable]
    public class PlayerIdPacket : Packet {
        public int PlayerId;

        public PlayerIdPacket(int pid) {
            PlayerId = pid;
        }
    }
}
using System;

namespace Llama.MORPG.Common.Packets {
    [Serializable]
    public class ChatPacket : Packet {
        public int PlayerId;
        public string Message;
        public string Username;

        public ChatPacket(int pid, string msg, string username = "") {
            PlayerId = pid;
            Message = msg;
            Username = username;
        }
        
    }
}
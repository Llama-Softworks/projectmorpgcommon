using System;

namespace Llama.MORPG.Common.Packets.QuestPackets {
    [Serializable]
    public class QuestStartPacket : Packet {
        public int QuestId;

        public QuestStartPacket(int qid) {
            QuestId = qid;
        }
    }
}
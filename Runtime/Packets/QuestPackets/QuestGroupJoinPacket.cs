using System;

namespace Llama.MORPG.Common.Packets.QuestPackets {
    [Serializable]
    public class QuestGroupJoinPacket : Packet {
        public int QuestGroupId;
        public int PlayerId;

        public QuestGroupJoinPacket(int qgid, int pid) {
            QuestGroupId = qgid;
            PlayerId = pid;
        }
    }
}
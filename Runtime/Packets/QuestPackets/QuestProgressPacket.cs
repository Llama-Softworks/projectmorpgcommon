using System;

namespace Llama.MORPG.Common.Packets.QuestPackets {
    [Serializable]
    public class QuestProgressPacket : Packet {
        public int QuestId;
        public int Progress;

        public QuestProgressPacket(int qid, int prog) {
            QuestId = qid;
            Progress = prog;
        }
    }
}
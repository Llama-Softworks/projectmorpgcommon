using System;

namespace Llama.MORPG.Common.Packets.QuestPackets {
    [Serializable]
    public class QuestGroupLeavePacket : Packet {
        public int QuestGroupId;
        public int PlayerId;

        public QuestGroupLeavePacket(int qgid, int pid) {
            QuestGroupId = qgid;
            PlayerId = pid;
        }
    }
}
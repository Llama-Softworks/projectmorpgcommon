using System;

namespace Llama.MORPG.Common.Packets.QuestPackets {
    [Serializable]
    public class QuestFinishPacket : Packet {
        public int QuestId;

        public QuestFinishPacket(int qid) {
            QuestId = qid;
        }
    }
}
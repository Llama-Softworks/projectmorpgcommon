using System;
using Llama.MORPG.Common.Quest.Goals;

namespace Llama.MORPG.Common.Packets.QuestPackets {
    [Serializable]
    public class QuestGoalCompletionPacket : Packet {
        public int QuestId;
        public int QuestProgress;
        public QuestGoal Goal;

        public QuestGoalCompletionPacket(int qid, int qp, QuestGoal g) {
            QuestId = qid;
            QuestProgress = qp;
            Goal = g;
        }
    }
}
using System;

namespace Llama.MORPG.Common.Packets {
    //Position as a Vector3
    [Serializable]
    public class SyncPacket : Packet {
        public int PlayerId;
        public float X;
        public float Y;
        public float Z;

        public SyncPacket(int pid, float x, float y, float z) {
            PlayerId = pid;
            X = x;
            Y = y;
            Z = z;
        }
    }
}
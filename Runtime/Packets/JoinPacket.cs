using System;

namespace Llama.MORPG.Common.Packets {
    [Serializable]
    public class JoinPacket : Packet {
        public int PlayerId;

        public JoinPacket(int pid) {
            PlayerId = pid;
        }
    }
}
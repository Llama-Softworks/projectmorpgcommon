using System;

namespace Llama.MORPG.Common.Packets {
    [Serializable]
    public class DisconnectPacket : Packet {
        public int PlayerId;

        public DisconnectPacket(int pid) {
            PlayerId = pid;
        }
    }
}
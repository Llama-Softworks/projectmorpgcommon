using System;

namespace Llama.MORPG.Common.Packets {
	[Serializable]
	public class MessagePacket : Packet {
		public string Message;

		public MessagePacket(string message) => Message = message;
	}
}
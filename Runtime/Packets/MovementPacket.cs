using System;

namespace Llama.MORPG.Common.Packets {
    //Movement as a Vector3
    [Serializable]
    public class MovementPacket : Packet {
        public int PlayerId;
        public float X;
        public float Y;
        public float Z;
        
        public MovementPacket(int pid, float x, float y, float z) {
            PlayerId = pid;
            X = x;
            Y = y;
            Z = z;
        }
    }
}
namespace Llama.MORPG.Common.Quest.Goals {
    public enum QuestGoalType {
        GatherGoal, KillGoal, TalkGoal
    }
}
using System;

namespace Llama.MORPG.Common.Quest.Goals {
    [Serializable]
    public class QuestGoal {
        public QuestGoalType GoalType;
        public bool Completed;
    }
}